package com.leafnode.bttest

import android.bluetooth.BluetoothDevice
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class DeviceRvAdapter(data: List<BluetoothDevice>) :
    RecyclerView.Adapter<DeviceRvAdapter.MyViewHolder>() {

    private val dataList = mutableListOf<BluetoothDevice>()

    init {
        dataList.addAll(data)
    }

    class MyViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        val device: TextView = view.findViewById(R.id.device)
    }

    fun setData(data: List<BluetoothDevice>) {
        dataList.addAll(data)
        notifyDataSetChanged()
    }

    fun addDevice(device: BluetoothDevice) {
        dataList.add(device)
        notifyDataSetChanged()

    }

    var listener: ((device: BluetoothDevice) -> Unit)? = null

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MyViewHolder {
        val textView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item, parent, false)
        return MyViewHolder(textView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.device.text =
            "address {${dataList[position].address}}, name {${dataList[position].name}}"
        holder.device.setOnClickListener {
            listener?.invoke(dataList[position])
        }
    }

    override fun getItemCount() = dataList.size
}
