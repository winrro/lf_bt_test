package com.leafnode.bttest


import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothManager
import android.bluetooth.le.*
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.io.UnsupportedEncodingException
import java.math.BigInteger
import java.util.*
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {

    private val deviceList = mutableListOf<BluetoothDevice>()
    private val mBluetoothAdapter: BluetoothAdapter by lazy {
        (getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter
    }
    private val mBluetoothLeScanner: BluetoothLeScanner by lazy { mBluetoothAdapter.bluetoothLeScanner }
    private val TAG = "LeafNode"
    private var mScanCallback: ScanCallback? = null
    private val SCAN_PERIOD: Long = 5000
    val rvAdapter = DeviceRvAdapter(mutableListOf())
    private val gatController = GatController(this)


    val testData = "DAT: 05/22/2020\n" +
            "    TIM: 12:07:18\n" +
            "    RTC: 12:07:17\n" +
            "    LOC: 33.66829,-117.85906\n" +
            "    DOP: 0.80\n" +
            "    SPD: 0 km/h\n" +
            "    HDG: 0\n" +
            "    SAT: 9\n" +
            "    LCK: 1\n" +
            "    FIX3:2,A\n" +
            "    FIXQ:1,A\n" +
            "    HDOP:0.80\n" +
            "    VDOP:0.80\n" +
            "    PDOP:1.20\n" +
            "    VSAT:13\n" +
            "    SNR: 42.85\n" +
            "    GPS: ON\n" +
            "    OK"

    val testData2 = "MCC,MNC:    310,260\n" +
            "    CellID:     428\n" +
            "    RSSI:       60\n" +
            "    Roaming:    0\n" +
            "    RX,TX:      -80,9\n" +
            "    Ec/Io,SINR: 0.0,65535\n" +
            "    BCx,Ch,PPN: 0,2300,NA\n" +
            "    Mode:       4G\n" +
            "    OK"

    fun parseResponseToMap(data: String): Map<String, String> {
        val map = mutableMapOf<String, String>()
        data.split("\n").forEach {
            val item = it.trim()
            if (!item.equals("ok", true)) {
                val dataArr = item.split(":")
                val key = dataArr[0].trim()
                val value = dataArr[1].trim()
                map[key] = value
            }
        }
        return map
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // startScanning()

        val data = parseResponseToMap(testData)
        val data2 = parseResponseToMap(testData2)
        Log.d("parsed", "data \n$data\n$data2")

        /* testData.split("\n").forEach {
             val item = it.trim()
             if (item.equals("ok", true)) {
                 Log.d("test parser item", "the end")
             } else {
                 val dataArr = item.split(":")
                 val key = dataArr[0]
                 val value = dataArr[1]
                 Log.d("test parser item", "key $key -> value ->$value")
             }
         }*/

        tvTitle.text = "Scanning BLE devices"
        rvList.layoutManager = LinearLayoutManager(this)

        rvAdapter.listener = {
            stopScanning()
            Toast.makeText(
                this,
                "Clicked to ${it.address}",
                Toast.LENGTH_SHORT
            ).show()
        }
        rvList.adapter = rvAdapter

        /* mHandler.postDelayed({
             Log.d(TAG, "device found - ${deviceList.size}")
             deviceList.forEachIndexed { index, bluetoothDevice ->
                 val uuid = bluetoothDevice.uuids?.toString() ?: "{null}"
                 val type = when (bluetoothDevice.type) {
                     DEVICE_TYPE_CLASSIC -> "DEVICE_TYPE_CLASSIC"
                     DEVICE_TYPE_LE -> "DEVICE_TYPE_LE"
                     DEVICE_TYPE_DUAL -> "DEVICE_TYPE_DUAL"
                     else -> "DEVICE_TYPE_UNKNOWN "
                 }
                 val msg =
                     "Device $index -> {${bluetoothDevice.name}} | {${bluetoothDevice.address}  Type - $type UUID - $uuid \n"
                 log(msg)
             }

             // startAdvertising()

             tvTitle.text = "Find ${deviceList.size} devices"
             rvAdapter.setData(deviceList)

         }, SCAN_PERIOD + 2000)*/
    }


    /*
    * Start scanning for BLE
    */
    fun startScanning() {
        if (mScanCallback == null) {
            tvTitle.text = "Scanning BLE devices"
            log("Starting Scanning")
            // Will stop the scanning after a set time.
            // mHandler.postDelayed({ stopScanning() }, SCAN_PERIOD)

            // Kick off a new scan.
            mScanCallback = SampleScanCallback()
            // mBluetoothLeScanner.startScan(buildScanFilters(), buildScanSettings(), mScanCallback)
            mBluetoothLeScanner.startScan(mScanCallback)
            val toastText = "Scanning for ${TimeUnit.SECONDS.convert(
                SCAN_PERIOD,
                TimeUnit.MILLISECONDS
            )} seconds"
            log(toastText)
        } else {
            log("Scanning already started")
        }
    }


    /*
    * Return a List of [ScanFilter] objects to filter by Service UUID.
    */
    private fun buildScanFilters(): List<ScanFilter>? {
        val scanFilters: MutableList<ScanFilter> =
            ArrayList()
        val builder = ScanFilter.Builder()
        // Comment out the below line to see all BLE devices around you
        // builder.setServiceUuid(UUID)
        scanFilters.add(builder.build())
        return scanFilters
    }

    /*
    * Return a [ScanSettings] object set to use low power (to preserve battery life).
    */
    private fun buildScanSettings(): ScanSettings? {
        val builder = ScanSettings.Builder()
        builder.setScanMode(ScanSettings.SCAN_MODE_LOW_POWER)
        return builder.build()
    }


    /**
     * Stop scanning for BLE Advertisements.
     */
    private fun stopScanning() {
        Log.d(TAG, "Stopping Scanning")
        // Stop the scan, wipe the callback.
        mBluetoothLeScanner.stopScan(mScanCallback)
        mScanCallback = null
    }


    fun getUUID(result: ScanResult): UUID? {
        val UUIDx = UUID.nameUUIDFromBytes(result.scanRecord!!.bytes)

        // val UUIDxx = result.scanRecord?.serviceUuids.toString()
        // log("UUID as String ->>$UUIDx")
        return UUIDx
    }

    inner class SampleScanCallback : ScanCallback() {
        override fun onBatchScanResults(results: List<ScanResult>) {
            super.onBatchScanResults(results)

            log("SampleScanCallback results count ${results.size}")

        }


        @ExperimentalCoroutinesApi
        override fun onScanResult(callbackType: Int, result: ScanResult) {
            super.onScanResult(callbackType, result)
            //  log("SampleScanCallback onScanResult  device {${result.device.address}} | {${result.device.name}}")
            if (deviceList.find { it.address == result.device.address } == null) {
                deviceList.add(result.device)
                rvAdapter.addDevice(result.device)
                log("Add new device ${result.device.address}  ${result.device.name}")
            }

            try {
                val byteArr = byteArrayOf(
                    result.scanRecord!!.bytes.get(2),
                    result.scanRecord!!.bytes.get(3),
                    result.scanRecord!!.bytes.get(4),
                    result.scanRecord!!.bytes.get(5),
                    result.scanRecord!!.bytes.get(6),
                    result.scanRecord!!.bytes.get(7),
                    result.scanRecord!!.bytes.get(8),
                    result.scanRecord!!.bytes.get(9)
                )
                val one = BigInteger(byteArr)
                val strResult: String = one.toString()


                val decodedData = Parser.bcdToString(result.scanRecord!!.bytes)
                val byteData = Parser.bcdToString(byteArr)
                //  log("data is $dd   ${result.device.address}")
                log("data $byteData  ${result.device.address}")

                if (decodedData.toString().contains("015115002386570")) {
                    //  if (result.device.address == "F5:4B:99:C8:D7:4E") {
                    stopScanning()
                    val uuid = getUUID(result)
                    log("found device $uuid ${result.device.address}")
                   // result.device.createBond()
                    Handler(Looper.getMainLooper()).postDelayed(
                        {
                            gatController.connect(result.device)
                            gatController.gatListener =
                                object : GatController.GatControllerListener {
                                    override fun onConnect(gat: BluetoothGatt) {

                                    }

                                    override fun onDisconnect(gat: BluetoothGatt) {

                                    }

                                    override fun oReadyToWork() {

                                    }

                                }
                        }, 2000
                    )

                }

            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
            }


        }

        override fun onScanFailed(errorCode: Int) {
            super.onScanFailed(errorCode)
            Log.d(TAG, "Scan failed with error: $errorCode")
        }
    }

    private fun log(message: String) {
        Log.d(TAG, message)
    }


}