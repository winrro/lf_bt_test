package com.leafnode.bttest

import android.bluetooth.*
import android.content.Context
import android.util.Log
import com.leafnode.bttest.BLECommand.CCCD
import com.leafnode.bttest.BLECommand.RX_SERVICE_UUID
import com.leafnode.bttest.BLECommand.read_uuid
import com.leafnode.bttest.BLECommand.write_uuid
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.nio.charset.Charset
import java.util.*


object BLECommand {
    val AT = "AT\r\n"
    val XINCEL = "AT+XINCEL\r\n"
    val XINGPS = "AT+XINGPS\r\n"
    val XINTPM = "AT+XINTPM\r\n"
    val XINABS = "AT+XINABS\r\n"

    var isATReceived = false
    var isXINCELreceived = false
    var isXINGPSreceived = false

    val read_uuid = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e")
    val write_uuid = UUID.fromString("6e400002-b5a3-f393-e0a9-e50e24dcca9e")
    val RX_SERVICE_UUID = UUID.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e")
    val CCCD = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")

}


@ExperimentalCoroutinesApi
//TODO make this class private
class CommandWorker {

    val channel = Channel<String>()

    var atListener: (() -> Unit)? = null
    var XINCELListener: (() -> Unit)? = null
    var XINGPSListener: (() -> Unit)? = null
    var XINABSListener: (() -> Unit)? = null
    var XINTPMListener: (() -> Unit)? = null

    private val logTag = "GatController CommandWorker"

    enum class BleQueryType {
        NONE, AT, XINCEL, XINGPS, XINABS, XINTPM
    }

    data class BleQuery(val type: BleQueryType, val data: String)

    var ATQuery = BleQuery(BleQueryType.AT, BLECommand.AT)
    var XINCELQuery = BleQuery(BleQueryType.XINCEL, BLECommand.XINCEL)
    var XINGPSQuery = BleQuery(BleQueryType.XINGPS, BLECommand.XINGPS)
    var XINTPMQuery = BleQuery(BleQueryType.XINTPM, BLECommand.XINTPM)
    var XINABSQuery = BleQuery(BleQueryType.XINABS, BLECommand.XINABS)

    enum class WorkStatus {
        IDLE, AT_START, AT_END, XINCEL_START, XINCEL_END, XINGPS_START, XINGPS_END, ERROR,
        XINABS_START, XINABS_END, XINTPM_START, XINTPM_END
    }

    private var atReceived = ""
    private var xinCelReceived: Map<String, String> = mutableMapOf()
    private var xinGpslReceived: Map<String, String> = mutableMapOf()
    private var xinAbslReceived: Map<String, String> = mutableMapOf()
    private var xinTpmlReceived: Map<String, String> = mutableMapOf()

    init {
        var inputDataString = ""
        GlobalScope.launch {
            channel.consumeEach { data ->
                when (runStatus) {
                    WorkStatus.AT_START -> {
                        Log.d(logTag, "AT_START received $data")
                        val clearData = data.replace("\\s".toRegex(), "")
                        if (data.contains("OK", true)) {
                            runStatus = WorkStatus.AT_END
                            runStatus = WorkStatus.IDLE
                            atReceived = clearData
                            Log.d(logTag, "AT ready \n $atReceived")
                            inputDataString = ""
                            atListener?.invoke()
                        }
                    }
                    WorkStatus.XINCEL_START -> {
                        Log.d(logTag, "XINCEL_START received $data")
                        inputDataString += data
                        if (data.contains("OK", true)) {
                            runStatus = WorkStatus.XINCEL_END
                            xinCelReceived = parseResponseToMap(inputDataString)
                            inputDataString = ""
                            runStatus = WorkStatus.IDLE
                            Log.d(logTag, "XINCEL ready \n ${xinCelReceived.toString()}")
                            XINCELListener?.invoke()
                        }
                    }
                    WorkStatus.XINGPS_START -> {
                        Log.d(logTag, "XINGPS_START received $data")
                        inputDataString += data
                        if (data.contains("OK", true)) {
                            runStatus = WorkStatus.XINGPS_END
                            xinGpslReceived = parseResponseToMap(inputDataString)
                            inputDataString = ""
                            runStatus = WorkStatus.IDLE
                            Log.d(logTag, "XINGP ready! \n ${xinGpslReceived.toString()}")
                            XINGPSListener?.invoke()
                        }
                    }
                    WorkStatus.XINABS_START -> {
                        Log.d(logTag, "XINABS_START received $data")
                        inputDataString += data
                        if (data.contains("OK", true)) {
                            runStatus = WorkStatus.XINABS_END
                            xinAbslReceived = parseResponseToMap(inputDataString)
                            inputDataString = ""
                            runStatus = WorkStatus.IDLE
                            Log.d(logTag, "XINABS ready! \n ${xinAbslReceived.toString()}")
                            XINABSListener?.invoke()
                        }
                    }
                    WorkStatus.XINTPM_START -> {
                        Log.d(logTag, "XINTPM_START received $data")
                        inputDataString += data
                        if (data.contains("OK", true)) {
                            runStatus = WorkStatus.XINTPM_END
                            xinTpmlReceived = parseResponseToMap(inputDataString)
                            inputDataString = ""
                            runStatus = WorkStatus.IDLE
                            Log.d(logTag, "XINTPM ready! \n ${xinTpmlReceived.toString()}")
                            XINTPMListener?.invoke()
                        }
                    }
                    else -> Log.d(logTag, "${runStatus.name} received $data")
                }
            }
        }
    }

    private fun parseResponseToMap(data: String): Map<String, String> {
        val map = mutableMapOf<String, String>()
        data.split("\n").forEach {
            try {
                val item = it.trim()
                if (!item.equals("ok", true)) {
                    val dataArr = item.split(":")
                    val key = dataArr[0].trim()
                    val value = dataArr[1].trim()
                    map[key] = value
                }
            } catch (e: Exception) {
            }
        }
        return map
    }

    var runStatus = WorkStatus.IDLE

    fun startCommand(command: BleQuery, gatt: BluetoothGatt) {
        if (runStatus == WorkStatus.IDLE) {
            startQuery(gatt, command)
        } else {
            Log.d(logTag, "Cannot start query, current status -> ${runStatus.name}")
        }
    }

    private fun startQuery(gatt: BluetoothGatt, command: BleQuery) {
        val commandToWrite = command.data

        val service = gatt.getService(RX_SERVICE_UUID)
        if (service == null) {
            Log.d(logTag, "cannot get service, service.uuid: $RX_SERVICE_UUID")
            return
        }
        val char = service.getCharacteristic(write_uuid)
        char.value = commandToWrite.toByteArray()
        val status = gatt.writeCharacteristic(char)

        runStatus = if (status) {
            when (command.type) {
                BleQueryType.AT -> WorkStatus.AT_START
                BleQueryType.XINCEL -> WorkStatus.XINCEL_START
                BleQueryType.XINGPS -> WorkStatus.XINGPS_START
                BleQueryType.XINABS -> WorkStatus.XINABS_START
                BleQueryType.XINTPM -> WorkStatus.XINTPM_START
                else -> WorkStatus.IDLE
            }
        } else {
            WorkStatus.ERROR
        }

        Log.d(logTag, "sent data $command isSend $status")
    }

}

@ExperimentalCoroutinesApi
class GatController(private val context: Context) {

    interface GatControllerListener {
        fun onConnect(gat: BluetoothGatt)
        fun onDisconnect(gat: BluetoothGatt)
        fun oReadyToWork()
    }

    var gatListener: GatControllerListener? = null

    private val commandWorker: CommandWorker = CommandWorker()
    private val logTag = "GatController"
    private var mBluetoothGatt: BluetoothGatt? = null

    private val mBluetoothAdapter: BluetoothAdapter by lazy {
        (context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter
    }

    fun connect(device: BluetoothDevice) {

        commandWorker.atListener = {
            mBluetoothGatt?.let { gatt ->
                commandWorker.startCommand(commandWorker.XINGPSQuery, gatt)
            }
        }

        commandWorker.XINGPSListener = {
            mBluetoothGatt?.let { gatt ->
                commandWorker.startCommand(commandWorker.XINCELQuery, gatt)
            }
        }
        commandWorker.XINCELListener = {
            mBluetoothGatt?.let { gatt ->
                commandWorker.startCommand(commandWorker.XINABSQuery, gatt)
            }
        }

        commandWorker.XINABSListener = {
            mBluetoothGatt?.let { gatt ->
                commandWorker.startCommand(commandWorker.XINTPMQuery, gatt)
            }
        }

        //val btDevice = mBluetoothAdapter.getRemoteDevice(device.address)
        val btDevice = device
        if (mBluetoothGatt == null) {
            Log.d(logTag, "connect to the bt device ${btDevice.address}")
            mBluetoothGatt =
                btDevice.connectGatt(context, false, mGattCallback, BluetoothDevice.TRANSPORT_LE)
        } else {
            Log.d(
                logTag,
                "mBluetoothGatt disconnect and connect again to the bt device ${btDevice.address}"
            )
            mBluetoothGatt?.disconnect()
            mBluetoothGatt =
                btDevice.connectGatt(context, false, mGattCallback, BluetoothDevice.TRANSPORT_LE)
        }
    }

    fun close() {
        mBluetoothGatt?.close()
        Log.d(logTag, "mBluetoothGatt close")
    }

    private fun enableNotifications() {
        mBluetoothGatt?.let { gat ->
            val RxService = gat.getService(RX_SERVICE_UUID)
            val TxChar = RxService.getCharacteristic(read_uuid)
            gat.setCharacteristicNotification(TxChar, true)
            Log.d(logTag, "enableNotifications done")
            val decsriptor = TxChar!!.getDescriptor(CCCD)
            if (decsriptor == null) {
                Log.d(logTag, "Could not get CCC descriptor for characteristics")
            }

            gatListener?.oReadyToWork()

/*
            Handler(Looper.getMainLooper()).postDelayed({
                decsriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                val res = gat.writeDescriptor(decsriptor)
                Log.d(logTag, "set descriptor status $res")

                Handler(Looper.getMainLooper()).postDelayed({
                    writeData()
                }, 2000)


            }, 2000)*/

            GlobalScope.launch {
                delay(2000)
                decsriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                val res = gat.writeDescriptor(decsriptor)
                Log.d(logTag, "set descriptor status $res")
                delay(2000)
                commandWorker.startCommand(commandWorker.ATQuery, gat)
            }


            //readCharacteristic(TxChar)
        }

    }

    fun readCharacteristic(characteristic: BluetoothGattCharacteristic?) {
        Log.d(logTag, "send readCharacteristic request ${characteristic?.uuid.toString()}")
        mBluetoothGatt?.readCharacteristic(characteristic)
    }


    private val mGattCallback: BluetoothGattCallback = object : BluetoothGattCallback() {
        override fun onConnectionStateChange(
            gatt: BluetoothGatt,
            status: Int,
            newState: Int
        ) {
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.d(logTag, "Connected to GATT server.")
                // Attempts to discover services after successful connection.

                /*  Handler(Looper.getMainLooper()).postDelayed({

                      Log.d(
                          logTag,
                          "Attempting to start service discovery:" + mBluetoothGatt?.discoverServices()
                      )
                  }, 300)
  */
                gatListener?.onConnect(gatt)
                GlobalScope.launch {
                    delay(500)
                    Log.d(logTag, "Start service discovery:" + mBluetoothGatt?.discoverServices())
                }

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.i(logTag, "Disconnected from GATT server.")
                close()
                gatListener?.onDisconnect(gatt)
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.d(
                    logTag,
                    "onServicesDiscovered mBluetoothGatt = $mBluetoothGatt, status: $status"
                )

                var serviceUuid: UUID? = null
                var charUUID: UUID? = null

                /* gatt.services.forEach { service ->
                     Log.d(
                         logTag,
                         "discovered service ${service.uuid} characteristics count is ${service.characteristics.size}"
                     )

                     service.characteristics.forEach { char ->
                         val data = if (char.value != null) {
                             String(char.value, Charsets.UTF_8)
                         } else {
                             "empty data"
                         }
                         Log.d(logTag, "char ${char.uuid} value $data}")

                         if (service.uuid.toString() == "6e400001-b5a3-f393-e0a9-e50e24dcca9e" && char.uuid.toString() == "6e400002-b5a3-f393-e0a9-e50e24dcca9e") {
                             serviceUuid = service.uuid
                             charUUID = char.uuid
                         }
                     }

                 }*/

                enableNotifications()


            } else {
                Log.d(logTag, "onServicesDiscovered received: $status")
            }
        }

        override fun onCharacteristicRead(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic,
            status: Int
        ) {
            Log.d(
                logTag,
                "onCharacteristicRead status $status, characteristic.uuid: ${characteristic.uuid}"
            )
            val data = String(characteristic.value, Charset.forName("UTF-8"))
            Log.d(logTag, " parsed data -> $data")

            Log.d(logTag, String.format("Received TX: %d", characteristic.value))

        }

        private var receivedXINCELString = ""
        private var XINCELCount = 0

        private var receivedxINGPSString = ""
        private var XINGPSCount = 0

        override fun onCharacteristicChanged(
            gatt: BluetoothGatt,
            characteristic: BluetoothGattCharacteristic
        ) {
            Log.d(logTag, "onCharacteristicChanged characteristic.uuid: ${characteristic.uuid}")


            val data = String(characteristic.value, Charset.forName("UTF-8"))

            GlobalScope.launch { commandWorker.channel.send(data) }

/*
            val clearData = data.replace("\\s".toRegex(), "")
            // Log.d(logTag, "received characteristic message $clearData")

            if (clearData.equals("ok", true) && !isATReceived) {
                isATReceived = true
                Log.d(logTag, "received characteristic message $clearData")
                Handler(Looper.getMainLooper()).postDelayed({
                    writeData(BLECommand.XINCEL)
                }, 3000)

            } else {
                Log.d(logTag, "received characteristic message $data")
                if (!isXINCELreceived) {
                    XINCELCount += 1
                    receivedXINCELString += data

                    if (XINCELCount == 6) {
                        Handler(Looper.getMainLooper()).postDelayed({
                            // Log.d(logTag, "parsing XINCEL done")
                            // Log.d(logTag, receivedXINCELString)
                            Log.d(
                                logTag,
                                "parsing XINCEL done->\n  ${parseResponseToMap(receivedXINCELString)}"
                            )
                            isXINCELreceived = true

                            writeData(BLECommand.XINGPS)

                        }, 5000)
                    }
                } else {

                    XINGPSCount += 1
                    receivedxINGPSString += data

                    if (XINGPSCount == 7) {
                        Handler(Looper.getMainLooper()).postDelayed({
                            //  Log.d(logTag, "parsing  XINGPS done")
                            //  Log.d(logTag, receivedxINGPSString)
                            Log.d(
                                logTag,
                                "parsing  XINGPS done->\n  ${parseResponseToMap(receivedxINGPSString)}"
                            )
                            // isXINGPSreceived = true

                            // writeData(BLECommand.XINGPS)

                        }, 5000)
                    }
                }

            }*/

        }

        override fun onCharacteristicWrite(
            gatt: BluetoothGatt?,
            characteristic: BluetoothGattCharacteristic?,
            status: Int
        ) {
            super.onCharacteristicWrite(gatt, characteristic, status)


            Log.d(logTag, "onCharacteristicWrite characteristic.uuid: ${characteristic!!.uuid}, status: $status")

            val data = String(characteristic.value, Charset.forName("UTF-8"))
            Log.d(logTag, "onCharacteristicWrite sent message $data")

/*
            Handler(Looper.getMainLooper()).postDelayed({
                val service = gatt!!.getService(RX_SERVICE_UUID)
                val char = service.getCharacteristic(read_uuid)
                readCharacteristic(char)
            },2000)*/

        }

    }

    fun writeData(command: String = BLECommand.AT) {
        Log.d(logTag, "prepare to write data, service.uuid: $RX_SERVICE_UUID")
        mBluetoothGatt?.let { gatt ->

            // commandWorker.startQuery(gatt)
            commandWorker.startCommand(commandWorker.ATQuery, gatt)


            /*  val service = gatt.getService(RX_SERVICE_UUID)
              if (service == null) {
                  Log.d(logTag, "cannot get service, service.uuid: $RX_SERVICE_UUID")
                  return
              }
              val char = service.getCharacteristic(write_uuid)
              *//* val command = "AT+XINVER\r\n"
             val byteData = command.toByteArray(Charsets.UTF_8)*//*
            char.value = command.toByteArray()
            val status = gatt.writeCharacteristic(char)
            Log.d(logTag, "sent data ${command} isSend $status")*/

        }
    }

    fun parseResponseToMap(data: String): Map<String, String> {
        val map = mutableMapOf<String, String>()
        data.split("\n").forEach {
            try {
                val item = it.trim()
                if (!item.equals("ok", true)) {
                    val dataArr = item.split(":")
                    val key = dataArr[0].trim()
                    val value = dataArr[1].trim()
                    map[key] = value
                }
            } catch (e: Exception) {
            }
        }
        return map
    }
}