package com.leafnode.bttest;

public class Parser {

    public static long bcdToDecimal(byte[] bcd) {
        return Long.parseLong(bcdToString(bcd));
    }

    public static String bcdToString(byte[] bcd) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < bcd.length; i++) {
            sb.append(bcdToString(bcd[i]));
        }

        return sb.toString();
    }


    public static String bcdToString(byte bcd) {
        StringBuilder sb = new StringBuilder();

        byte high = (byte) (bcd & 0xf0);
        high >>>= (byte) 4;
        high = (byte) (high & 0x0f);
        byte low = (byte) (bcd & 0x0f);

        sb.append(high);
        sb.append(low);

        return sb.toString();
    }

}
